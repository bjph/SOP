export default {
  /*
   @param el 指令所绑定的元素
   @param binding {Object}
   @param vnode vue编译生成的虚拟节点
   */
  bind (el, binding, vnode) {
    const documentHandler = function (e) {
      if (!vnode.context || el.contains(e.target)) {
        return false
      }
      el.clickoutsideContext.bindingFn(e)
    }
    el.clickoutsideContext = {
      documentHandler,
      bindingFn: binding
    }
    setTimeout(() => {
      document.addEventListener('click', documentHandler)
    }, 0)
  },
  unbind (el) {
    document.removeEventListener('click', el.clickoutsideContext.documentHandler)
  }
}
