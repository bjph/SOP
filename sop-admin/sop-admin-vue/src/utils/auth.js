import Cookies from 'js-cookie'

export function getToken(TokenKey = 'sop-admin-token') {
  return Cookies.get(TokenKey)
}

export function setToken(token, TokenKey = 'sop-admin-token') {
  return Cookies.set(TokenKey, token, {expires: 0.3})
}

export function removeToken(TokenKey = 'sop-admin-token') {
  return Cookies.remove(TokenKey)
}
