import Vue from 'vue'
import XEUtils from 'xe-utils'
import {
  VXETable,
  Header,
  Column,
  Table,
  Footer,
  Toolbar,
  Pager,
  Select
} from 'vxe-table'
import zhCN from 'vxe-table/lib/locale/lang/zh-CN'

console.log(VXETable);

// 导入默认的国际化（如果项目中使用多语言，则应该导入到 vue-i18n 中）
VXETable.setup({
  i18n: key => XEUtils.get(zhCN, key)
})

VXETable.interceptor.add('event.clearActived', (params, e) => {
  // 比如点击了某个组件的弹出层面板之后，此时被激活单元格不应该被自动关闭，通过返回 false 可以阻止默认的行为。
  const doms = document.getElementsByClassName('other-dialog');
  for (let i = 0; i < doms.length; i++) {
    if (doms[i].contains(e.target)) {
      return false
    }
  }
  return true
})

// 先按需导入依赖的模块
Vue.use(Header)
Vue.use(Column)
Vue.use(Footer)
Vue.use(Toolbar)
Vue.use(Pager)
Vue.use(Select)
// 最后安装核心库
Vue.use(Table)
