import request from '@/utils/request'

export function getOrganListByPage(data) {
  console.log(document.cookie)
  return request({
    url: 'ihomecspweb/scporgan/organListByPage',
    method: 'post',
    data: {...data.params}
  })
}
