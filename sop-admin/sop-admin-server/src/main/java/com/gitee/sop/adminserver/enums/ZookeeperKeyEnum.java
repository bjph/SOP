package com.gitee.sop.adminserver.enums;

import lombok.Getter;

@Getter
public enum ZookeeperKeyEnum {


    META(1, "instance_%s_meta"),
    ENV(2, "env"),
    STATUS(3, "status"),;

    private Integer code;
    private String key;

    ZookeeperKeyEnum(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    public static String getDescByCode(Integer code) {
        if (code == null) {
            return "";
        }
        for (ZookeeperKeyEnum payStatusEnum : ZookeeperKeyEnum.values()) {
            if (code.equals(payStatusEnum.getCode())) {
                return payStatusEnum.key;
            }
        }
        return "";
    }

}