package com.gitee.sop.adminserver.freeway.bean.ums;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName UserResult.java
 * @Description TODO
 * @createTime 2022年01月04日 14:13:00
 */
@Data
@ToString
public class UserResult implements Serializable {
    /**
     * 主键
     */
    private String  id;
    /**
     * 账号
     */
    private String  username;
    /**
     * 机构id
     */
    private String  organId;
    /**
     * 所属根机构id
     */
    private String  rootOrganId;
    /**
     * 所属部门id
     */
    private String  deptId;
    /**
     * 员工上级/汇报对象
     */
    private String  parentId;
    /**
     * 状态： 0-禁用 1-启用
     */
    private Integer isEnable;

    /**
     * 用户姓名
     */
    private String  name;
    /**
     * 用户昵称
     */
    private String  nickname;
    /**
     * 员工编号，由企业自己统一编号
     */
    private String  code;
    /**
     * 账号类型：0-员工 1- 客户 2-安装工人 3-供应商
     */
    private Integer type;

    /**
     * 头像
     */
    private String  avatar;
    /**
     * 生日
     */
    private Date birthDate;
    /**
     * 性别：0-男，1-女，2-空值
     */
    private Integer sex;

    /**
     * 手机号码
     */
    private String  mobile;
    /**
     * 邮箱
     */
    private String  email;
    /**
     * qq
     */
    private String qq;
    /**
     * 钉钉用户userid
     */
    private String openId;
    /**
     * 钉钉用户昵称
     */
    private String openName;
    /**
     * 钉钉用户unionid
     */
    private String unionId;
    /**
     * 淘宝用户userid
     */
    private String tbUserId;
    /**
     * 备注
     */
    private String  remark;
    /**
     * 最后登录时间
     */
    private Date    lastLoginTime;
    /**
     * 生效时间
     */
    private Date    beginDate;
    /**
     * 失效时间
     */
    private Date    endDate;
    /**
     * 是否平台用户
     */
    private Integer isPlatform;
    /**
     * 创建时间
     */
    private Date    createDate;
    /**
     * 创建人
     */
    private String  creator;
    /**
     * 修改时间
     */
    private Date    modifyDate;
    /**
     * 修改人
     */
    private String  modify;
    /**
     * 来源
     */
    private Integer origin;

    /**
     * 数据来源id，与origin搭配使用，可用于关联第三方系统
     */
    private String relationId;

    /**
     * 钉钉企业ID
     */
    private String corpId;

    /**
     * 阿里EAid
     */
    private String aliEaId;

    /**
     * 阿里EA用户机构id
     */
    private String aliEntId;

    /**
     * 密码所属系统，用于判断当前面加密方式
     */
    private Integer passwordBelongs;
}
