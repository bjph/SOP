package com.gitee.sop.adminserver.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.gitee.easyopen.ApiConfig;
import com.gitee.easyopen.ApiParam;
import com.gitee.easyopen.ApiParamParser;
import com.gitee.easyopen.ParamNames;
import com.gitee.easyopen.interceptor.ApiInterceptor;
import com.gitee.easyopen.session.ApiSessionManager;
import com.gitee.sop.adminserver.helper.ZookeeperHelper;
import com.gitee.sop.adminserver.interceptor.LoginInterceptor;
import com.gitee.sop.adminserver.service.RegistryService;
import com.gitee.sop.adminserver.service.impl.RegistryServiceEurekaImpl;
import com.gitee.sop.adminserver.service.impl.RegistryServiceNacosImpl;
import com.gitee.sop.adminserver.service.impl.RegistryServiceZookeeperImpl;
import com.i72.basic.BasicMarkter;
import com.i72.basic.SOAServiceAddress;
import com.i72.basic.SOAServiceCenter;
import com.i72.freeway.FreewayMarkter;
import com.i72.freeway.ProxyServiceProcessor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;


/**
 * @author tanghc
 */
@Configuration
public class WebConfig {

    public static final String HEADER_TOKEN_NAME = ParamNames.ACCESS_TOKEN_NAME;

    @Value("${admin.access-token.timeout-minutes}")
    private String accessTokenTimeout;

    @Bean
    ApiConfig apiConfig() {
        ApiConfig apiConfig = new ApiConfig();
        apiConfig.setJsonResultSerializer(obj -> JSON.toJSONString(obj,
                SerializerFeature.WriteNullStringAsEmpty
                , SerializerFeature.WriteMapNullValue
                , SerializerFeature.WriteDateUseDateFormat)
        );

        ApiSessionManager apiSessionManager = new ApiSessionManager();
        // session有效期
        int timeout = NumberUtils.toInt(accessTokenTimeout, 30);
        apiSessionManager.setSessionTimeout(timeout);
        apiConfig.setSessionManager(apiSessionManager);
        // 登录拦截器
        apiConfig.setInterceptors(new ApiInterceptor[]{new LoginInterceptor()});

        apiConfig.setParamParser(new ApiParamParser() {
            @Override
            public ApiParam parse(HttpServletRequest request) {
                ApiParam param = super.parse(request);
                String accessToken = request.getHeader(HEADER_TOKEN_NAME);
                if (StringUtils.isNotBlank(accessToken)) {
                    param.put(ParamNames.ACCESS_TOKEN_NAME, accessToken);
                }
                return param;
            }
        });

        return apiConfig;
    }

    /**
     * 当配置了registry.name=nacos生效。没有配置同样生效
     *
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "registry.name", havingValue = "nacos", matchIfMissing = true)
    RegistryService registryServiceNacos() {
        return new RegistryServiceNacosImpl();
    }


    @ConditionalOnProperty(value = "registry.name", havingValue = "zookeeper")
    @Configuration
    class ZookeeperConfiguration{
        @Bean
        RegistryService registryServiceZookeeper() {
            return new RegistryServiceZookeeperImpl();
        }

        /*
        @Bean
        ZookeeperHelper zookeeperHelper(){
            return new ZookeeperHelper();
        }*/
    }



    /**
     * 当配置了registry.name=eureka生效。
     *
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "registry.name", havingValue = "eureka")
    RegistryService registryServiceEureka() {
        return new RegistryServiceEurekaImpl();
    }


    @Bean
    public BasicMarkter basicMarkter(){
        return new BasicMarkter();
    }

    @Bean
    public FreewayMarkter freewayMarkter(){
        return new FreewayMarkter();
    }



    /*
    @Bean
    public WebMvcConfigurer corsConfigurer()
    {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").
                        allowedOrigins("*"). //允许跨域的域名，可以用*表示允许任何域名使用
                        allowedMethods("GET","POST","DELETE"). //允许任何方法（post、get等）
                        allowedHeaders("*"). //允许任何请求头
                        allowCredentials(true).
                        exposedHeaders("Access-Control-Allow-Origin");//. //带上cookie信息
                        //exposedHeaders(HttpHeaders.SET_COOKIE).maxAge(3600L); //maxAge(3600)表明在3600秒内，不需要再发送预检验请求，可以缓存该结果
            }
        };
    }*/

    /*
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("authorization", "content-type", "x-auth-token"));
        configuration.setExposedHeaders(Arrays.asList("x-auth-token"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return new CorsFilter(source);
    }*/

}