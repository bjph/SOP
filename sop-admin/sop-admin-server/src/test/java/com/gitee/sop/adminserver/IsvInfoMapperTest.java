package com.gitee.sop.adminserver;

import com.gitee.sop.adminserver.api.isv.result.IsvDetailDTO;
import com.gitee.sop.adminserver.freeway.api.ums.UmUserService;
import com.gitee.sop.adminserver.freeway.api.zaorder.OrderCenterService;
import com.gitee.sop.adminserver.freeway.bean.ums.UserResult;
import com.gitee.sop.adminserver.freeway.bean.zaorder.OrderLogisticsRespDTO;
import com.gitee.sop.adminserver.mapper.IsvInfoMapper;
import com.i72.freeway.FreewayCallContext;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author tanghc
 */
public class IsvInfoMapperTest extends SopAdminServerApplicationTests {

    @Autowired
    IsvInfoMapper isvInfoMapper;

    @Test
    public void testGet() {
        IsvDetailDTO isvDetail = isvInfoMapper.getIsvDetail("2019032617262200001");
        System.out.println(isvDetail);
    }

    @Autowired
    private OrderCenterService orderCenterService;

    @Autowired
    private UmUserService umUserService;

    @Test
    public void testFreeway(){

        FreewayCallContext.set("jj","kk");

        OrderLogisticsRespDTO orderLogisticsRespDTO = orderCenterService.orderLogistics(7302l);
        String v = "";
    }

    @Test
    public void testum(){
        UserResult userResult = umUserService.getByToken("9d7b6995f8c14c0b9cb61a5aec3ff03b");
        String v = "";
    }
}
