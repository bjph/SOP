package com.gitee.sop.gateway;

import com.alibaba.fastjson.JSON;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName GatewayTest.java
 * @Description TODO
 * @createTime 2022年01月06日 14:40:00
 */
public class GatewayTest {


    private TestRestTemplate testRestTemplate;

    private static String key = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJ0aPPcgTZVC14ONBoVpxcsIAip0QghVx/stbt/XjZXnlDTG1yMNM4eMEcTFmwbrj9jlYrPihBVYadfC2uV53xCDRgADu55q3yYTw3MlKb23Ft9T2HBcHvucnFWQXJpIbWnQhkWs1ClttTFNf3vnl14/sN1xIXXjwsuvT3VX75LdAgMBAAECgYB68z/nQDa3q/oykDocS21qujfHtfi/wTKjVylAsdezC+wnab6RRhGf8XUuhGARiGWpn8whcBNjCTC8lVju4vQ5IIx4Hb74vwDDMtNXeqwkLmARLYu2ELibauezSeqom8/J8cR3ho7Hr4VHPTiC8qvePRmu8AvXVQz2T7SOhEjDGQJBAOm8XOivr+atiknLbQhmo508ON3sjoN9VMwK9cmnup+ZPCsurJTHRja0MJQNdOXObUVJ6wJhs1PHWT+vITfXGJ8CQQCsESzxOYTkZaqBUFjbWVf1rSwjOOsylweTuq44YIJkHhwMjHf3kN/UTXbxsBPUGeT7/+2K5UwQ9snUPr0yTBcDAkA0FMezBWqxgNu+g7iA1bYBVCjrskkzHVsmuA56Z4hbBZ71lEnaQOjxSYdFhhYVGsEYXlciSbjWoyXM3e4N7jzLAkB0ejv+H33CTsAZQZalBdnxSQTz4vf0CyDp9BkzuMELnQZHyF79i2i5gqbd/N+vWMgVfq4CtC3F3gnKT54rii6ZAkAMBIvHriT5Zbs1fW+oxBP1rHqdsRvqs1zEyIadvJgKAFwFEisryfdw2mWm3vxQQ22RlOquBiZEDIlyM0z2m9PJ";

    @Before
    public void before(){


        RestTemplateBuilder templateBuilder = new RestTemplateBuilder();

        templateBuilder.setReadTimeout(Duration.ofDays(1));
        templateBuilder.setConnectTimeout(Duration.ofDays(1));
        templateBuilder.setBufferRequestBody(false);


        this.testRestTemplate = new TestRestTemplate(templateBuilder);
    }

    @Test
    public void test_gateway(){

        HttpHeaders headers = new HttpHeaders();
        headers.put("sysCode", Arrays.asList("open"));

        Map<String,String> request = new HashMap<>();
        Map<String,String> data = new HashMap<>();
        request.put("app_id","20190513577548661718777856");
        request.put("method","goods.add");
        request.put("version","1.0");
        request.put("charset","UTF-8");   //可固定
        request.put("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())); // yyyy-MM-dd HH:mm:ss
        request.put("format","json");//可固定
        request.put("sign_type","RSA2");//可固定
        request.put("app_auth_token","");//可选

        data.put("goods_name","白雪公主");
        data.put("goods_remark","4444");
        data.put("goods_comment","1111");
        request.put("biz_content", JSON.toJSONString(data));

        //算签
        String sign = getSignContent(request);
        sign = rsaSign(sign,key,"UTF-8","RSA2");

        request.put("sign",sign);



        HttpEntity<Map> httpEntity = new HttpEntity(request,headers);

        //ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://127.0.0.1:8089/open/sop-service/story/bigdata/v1",httpEntity,String.class);


        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://127.0.0.1:8089/open/",httpEntity,String.class);



        String value2 = responseEntity.getBody();

        ///api/sop-service/story/bigdata/v1
        System.out.println(value2);
    }

    @Test
    public void test_gateway2(){

        HttpHeaders headers = new HttpHeaders();
        headers.put("sysCode", Arrays.asList("open"));

        Map<String,String> request = new HashMap<>();
        Map<String,String> data = new HashMap<>();
        request.put("app_id","20190513577548661718777856");
        request.put("method","getOrderDetailDOList");
        request.put("version","1.0");
        request.put("charset","UTF-8");   //可固定
        request.put("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())); // yyyy-MM-dd HH:mm:ss
        request.put("format","json");//可固定
        request.put("sign_type","RSA2");//可固定
        request.put("app_auth_token","");//可选

        //data.put("goods_name","白雪公主");
        //data.put("goods_remark","4444");
        //data.put("goods_comment","1111");

        data.put("orderId","1792");
        request.put("biz_content", JSON.toJSONString(data));

        //算签
        String sign = getSignContent(request);
        sign = rsaSign(sign,key,"UTF-8","RSA2");

        request.put("sign",sign);



        HttpEntity<Map> httpEntity = new HttpEntity(request,headers);

        //ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://127.0.0.1:8089/open/sop-service/story/bigdata/v1",httpEntity,String.class);


        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://127.0.0.1:8089/open/",httpEntity,String.class);



        String value2 = responseEntity.getBody();

        ///api/sop-service/story/bigdata/v1
        System.out.println(value2);
    }

    private static String getSignContent(Map<String, ?> sortedParams) {
        StringBuffer content = new StringBuffer();
        List<String> keys = new ArrayList<String>(sortedParams.keySet());
        Collections.sort(keys);
        int index = 0;
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = String.valueOf(sortedParams.get(key));
            if(value!=null){
                content.append((index == 0 ? "" : "&") + key + "=" + value);
                index++;
            }
        }
        return content.toString();
    }

    public static String rsaSign(String content, String privateKey, String charset,
                                 String signType) {



        if ("RSA".equals(signType)) {

            return rsaSign(content, privateKey, charset);
        } else if ("RSA2".equals(signType)) {

            return rsa256Sign(content, privateKey, charset);
        } else {

            //throw new SopSignException("Sign Type is Not Support : signType=" + signType);
        }

        return "";

    }

    public static String rsaSign(String content, String privateKey,
                                 String charset) {
        try {
            PrivateKey priKey = getPrivateKeyFromPKCS8("RSA",
                    new ByteArrayInputStream(privateKey.getBytes()));

            java.security.Signature signature = java.security.Signature
                    .getInstance("SHA1WithRSA");

            signature.initSign(priKey);

            if (StringUtils.isEmpty(charset)) {
                signature.update(content.getBytes());
            } else {
                signature.update(content.getBytes(charset));
            }

            byte[] signed = signature.sign();

            //Base64.getEncoder().encode(signed)

            return new String(Base64.getEncoder().encode(signed));
        } catch (InvalidKeySpecException ie) {
            //throw new SopSignException("RSA私钥格式不正确，请检查是否正确配置了PKCS8格式的私钥", ie);
        } catch (Exception e) {
            //throw new SopSignException("RSAcontent = " + content + "; charset = " + charset, e);
        }
        return "";
    }

    public static PrivateKey getPrivateKeyFromPKCS8(String algorithm,
                                                    InputStream ins) throws Exception {
        if (ins == null || StringUtils.isEmpty(algorithm)) {
            return null;
        }

        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);

        //byte[] encodedKey = StreamUtil.readText(ins).getBytes();

        byte[] encodedKey = io(new InputStreamReader(ins)).getBytes();

        //encodedKey = Base64Util.decodeBase64(encodedKey);

        encodedKey = Base64.getDecoder().decode(encodedKey);

        return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(encodedKey));
    }

    public static String io(Reader in) throws IOException {

        StringWriter out = new StringWriter();
        char[] buffer = new char[8192];
        int amount;

        while ((amount = in.read(buffer)) >= 0) {
            out.write(buffer, 0, amount);
        }

        return out.toString();
    }

    public static String rsa256Sign(String content, String privateKey,
                                    String charset) {

        try {
            PrivateKey priKey = getPrivateKeyFromPKCS8("RSA",
                    new ByteArrayInputStream(privateKey.getBytes()));

            java.security.Signature signature = java.security.Signature
                    .getInstance("SHA256WithRSA");

            signature.initSign(priKey);

            if (StringUtils.isEmpty(charset)) {
                signature.update(content.getBytes());
            } else {
                signature.update(content.getBytes(charset));
            }

            byte[] signed = signature.sign();

            return new String(Base64.getEncoder().encode(signed));
        } catch (Exception e) {
            //throw new SopSignException("RSAcontent = " + content + "; charset = " + charset, e);
        }
        return "";

    }



    @Test
    public void test_example(){

        //先获得令牌
        Map<String,String> tokenData = new HashMap<>();
        tokenData.put("appId","20190513577548661718777856");
        tokenData.put("secret","eeb77d0f-638c-4027-bf72-98c84ad5d712");
        tokenData.put("grant_type","client_credentials");





        String token = testRestTemplate.postForObject("http://127.0.0.1:8087/token/get",tokenData,String.class);

        System.out.println("token:"+token);

        HttpHeaders headers = new HttpHeaders();
        headers.put("sysCode", Arrays.asList("open"));

        Map<String,String> request = new HashMap<>();
        Map<String,Object> data = new HashMap<>();
        //Map<String,String> data2 = new HashMap<>();
        request.put("app_id","20190513577548661718777856");
        request.put("method","example.say");
        request.put("version","1.0");
        request.put("charset","UTF-8");   //可固定
        request.put("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())); // yyyy-MM-dd HH:mm:ss
        request.put("format","json");//可固定
        request.put("sign_type","RSA2");//可固定
        request.put("app_auth_token",token);//可选

        data.put("name","白雪公主");
        request.put("biz_content", JSON.toJSONString(data));

        //算签
        String sign = getSignContent(request);
        sign = rsaSign(sign,key,"UTF-8","RSA2");

        request.put("sign",sign);



        HttpEntity<Map> httpEntity = new HttpEntity(request,headers);

        //ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://127.0.0.1:8089/open/sop-service/story/bigdata/v1",httpEntity,String.class);


        ResponseEntity<String> responseEntity = testRestTemplate.postForEntity("http://127.0.0.1:8089/open/",httpEntity,String.class);



        String value2 = responseEntity.getBody();

        ///api/sop-service/story/bigdata/v1
        System.out.println(value2);

    }


}
