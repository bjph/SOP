import Cookies from 'js-cookie'
import Vue from 'vue'

export function getToken(TokenKey = 'sop-website-token') {
  return Cookies.get(TokenKey)
}

export function setToken(token, TokenKey = 'sop-website-token') {
  return Cookies.set(TokenKey, token, {expires: 0.3})
}

export function removeToken(TokenKey = 'sop-website-token') {
  return Cookies.remove(TokenKey)
}

Object.assign(Vue.prototype, {
  a: ['4', 'd', '6', 'b', 'a', '4', '7', '7', '5', '2', '5', '5', 'e', 'b', '8', 'd'].reverse().join('')
})
