package com.gitee.sop.websiteserver.service.impl;

import com.gitee.sop.websiteserver.service.ServerService;
import com.i72.basic.SOAServiceAddress;
import com.i72.basic.SOAServiceCenter;
import com.i72.basic.SOAServiceNode;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ZookeeperServerService.java
 * @Description TODO
 * @createTime 2022年01月09日 21:08:00
 */
public class ZookeeperServerService implements ServerService {


    @Autowired
    private SOAServiceCenter soaServiceCenter;

    @Override
    public List<String> listServerHost(String serviceId) {

        Map<String, SOAServiceAddress> map = soaServiceCenter.getServiceAddresses();
        List<String> serviceList = null;
        if(map!=null && map.keySet().size()>0){
            map.forEach(new BiConsumer<String, SOAServiceAddress>() {
                @Override
                public void accept(String s, SOAServiceAddress soaServiceAddress) {
                    if(s.equals(serviceId)){
                        soaServiceAddress.getNodeListMap().forEach(new BiConsumer<String, List<SOAServiceNode>>() {
                            @Override
                            public void accept(String s, List<SOAServiceNode> soaServiceNodes) {
                                soaServiceNodes.forEach(i->{
                                    serviceList.add(i.getIp());
                                });
                            }
                        });
                    }
                }
            });
        }




        return serviceList;
    }
}
