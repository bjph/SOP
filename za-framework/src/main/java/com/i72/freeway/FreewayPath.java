package com.i72.freeway;

import com.alibaba.fastjson.serializer.SerializerFeature;

import java.lang.annotation.*;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName FreewayPath.java
 * @Description TODO
 * @createTime 2021年12月28日 14:20:00
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FreewayPath {

    String value() default "";

    /**
     * 接口超时时间 ms
     */
    int timeout() default 0;

    /**
     * 接口方法转发路径
     */
    String forward() default "";

    /**
     * 接口方法是否公开
     */
    boolean isOpen() default true;

    /**
     * 指定服务节点
     */
    String serviceNode() default "";

    /**
     * 接口是否在网关开放
     */
    boolean isDoorGoodOpen() default true;

    /**
     * 序列化拦截器
     * @see com.alibaba.fastjson.serializer.SerializeFilter
     */
    Class<?>[] serializeFilters() default {};

    /**
     * 序列化配置项
     */
    SerializerFeature[] serializerFeatures() default {};


    boolean isResultWrap() default true;

}
