package com.i72.freeway;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName FreewayConfig.java
 * @Description TODO
 * @createTime 2021年12月28日 14:52:00
 */
public class FreewayConfig {


    /**
     * 服务映射
     */
    private static Map<String, String> serviceMappings;

    /**
     * 本应用的Freeway接口列表
     */
    private static Set<Class> appFreewayApiList = new LinkedHashSet<>();

    public static Map<String, String> getServiceMappings() {
        if (serviceMappings == null) {
            //serviceMappings = SwjConfig.getPropertiesValueMap("swj.freeway.service-mappings.");
        }
        return serviceMappings;
    }

    public static void addAppFreewayApi(Class clazz) {
        appFreewayApiList.add(clazz);
    }

    public static Set<Class> getAppFreewayApiList() {
        return appFreewayApiList;
    }

}
