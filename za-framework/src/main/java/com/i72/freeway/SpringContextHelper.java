package com.i72.freeway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName SpringContextHelper.java
 * @Description TODO
 * @createTime 2021年12月28日 14:50:00
 */
@Slf4j
public class SpringContextHelper implements ApplicationContextAware {

    private static ApplicationContext applicationContext;
    private static BeanFactory beanFactory;

    /**
     * 获取当前上下文
     *
     * @return ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * 获取bean
     *
     * @param name bean名称
     * @return Object
     * @throws BeansException BeansException
     */
    public static Object getBean(String name) throws BeansException {
        return applicationContext.getBean(name);
    }

    /**
     * 获取bean
     *
     * @param name bean名称
     * @return Object
     * @throws BeansException BeansException
     */
    public static Object getBean(String name, boolean required) throws BeansException {
        try {
            return applicationContext.getBean(name);
        } catch (NoSuchBeanDefinitionException e) {
            log.warn("No Such BeanDefinition: {}", name);
            if (required) throw e;
            return null;
        }
    }

    /**
     * 获取bean
     *
     * @param clazz 类型
     * @return Object
     * @throws BeansException BeansException
     */
    public static <T> T getBeanByType(Class<T> clazz) throws BeansException {
        return applicationContext.getBean(clazz);
    }

    /**
     * 获取bean
     *
     * @param clazz    类型
     * @param required 是否必须存在该bean
     * @return Object
     * @throws BeansException BeansException
     */
    public static <T> T getBeanByType(Class<T> clazz, boolean required) throws BeansException {
        try {
            return applicationContext.getBean(clazz);
        } catch (NoSuchBeanDefinitionException e) {
            log.warn("No Such BeanDefinition: {}", clazz.getName());
            if (required) throw e;
            return null;
        }
    }

    /**
     * 获取beanFactory，可用于注册bean
     *
     * @return DefaultListableBeanFactory beanFactory
     */
    public static BeanFactory getBeanFactory() {
        return beanFactory;
    }

    /**
     * 获取ConfigurableEnvironment
     */
    public static Environment getEnvironment() {
        return applicationContext.getEnvironment();
    }



    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextHelper.applicationContext = applicationContext;
        SpringContextHelper.beanFactory = applicationContext.getAutowireCapableBeanFactory();
    }
}
