package com.i72.freeway;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName FreewayCallContext.java
 * @Description TODO
 * @createTime 2022年01月15日 10:39:00
 */
public class FreewayCallContext {

    private static ThreadLocal<Map<String,String>> LOCAL = new ThreadLocal<>();

    public static void set(String key,String value){
        if(LOCAL.get()==null){
            LOCAL.set(new HashMap<>());
        }
        Map<String,String> map = LOCAL.get();

        map.put(key,value);
    }

    public static Map<String,String> getCallContext(){
        Map<String,String> map = LOCAL.get();
        return map;
    }

    public static void removeAll(){
        LOCAL.remove();
    }

    public static String remove(String key){
        Map<String,String> map = LOCAL.get();
        if(map!=null){
            return map.remove(key);
        }
        return null;
    }

}
