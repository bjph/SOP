package com.i72.freeway;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ProxyModel.java
 * @Description TODO
 * @createTime 2021年12月28日 14:54:00
 */
@Getter
@Setter
@AllArgsConstructor
public class ProxyModel {
    private String beanName;

    private Class clazz;

    private Class proxyClass;
}
