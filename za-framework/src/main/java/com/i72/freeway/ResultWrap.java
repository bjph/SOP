package com.i72.freeway;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ResultWrap.java
 * @Description TODO
 * @createTime 2021年12月28日 16:16:00
 */
@Getter
@Setter
@ToString
public class ResultWrap<T> implements Serializable {

    private boolean success = false;

    private T result;

    private Integer errorCode;

    private String errorMessage;

    private String traceId;

    public ResultWrap() {}

    public ResultWrap(T result) {
        this.success = true;
        this.result = result;
    }

    public ResultWrap(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

}
