package com.i72.freeway;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName HandlerContext.java
 * @Description TODO
 * @createTime 2021年12月28日 16:42:00
 */
@Getter
@Setter
public class HandlerContext {

    private String serviceAddress;

    /**
     * rpc调用发送的请求头
     */
    private Map<String, String> headers;

    /*------ 以下copy from handleParam ------*/
    /**
     * rpc的服务节点名称
     */
    private String serviceName;

    /**
     * rpc服务接口地址，格式：apiName/methodName
     */
    private String servicePath;

    /**
     * rpc服务接口的版本号
     */
    private String version;

    /**
     * 接口的超时时间
     */
    private int timeout;

}
