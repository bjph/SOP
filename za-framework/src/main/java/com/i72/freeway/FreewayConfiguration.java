package com.i72.freeway;

import com.i72.basic.BasicConfiguration;
import com.i72.basic.BasicMarkter;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName FreewayConfiguration.java
 * @Description TODO
 * @createTime 2021年12月29日 14:43:00
 */
@Configuration
@ConditionalOnBean(FreewayMarkter.class)
public class FreewayConfiguration {

    /*
    @Bean
    public SpringContextHelper getSpringContextHelper(){
        return new SpringContextHelper();
    }*/

    @Bean
    public ProxyServiceProcessor getProxyServiceProcessor(){
        return new ProxyServiceProcessor();
    }



}
