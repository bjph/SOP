package com.i72.basic;

import lombok.ToString;

import java.util.Objects;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName SOAServiceNode.java
 * @Description TODO
 * @createTime 2021年12月28日 18:02:00
 */
@ToString
public class SOAServiceNode {

    /** 节点IP */
    private String ip;

    /** 被调用次数 */
    private int times;

    private SOAServiceCenter.NodeProperties nodeProperties;

    private String serviceName;

    SOAServiceNode(String ip, SOAServiceCenter.NodeProperties nodeProperties) {
        this.ip = ip;
        this.nodeProperties = nodeProperties;
    }

    /**
     * 获取节点IP
     */
    public String getIp() {
        times++;
        return ip;
    }

    /**
     * 获取该节点的调用次数
     */
    public int getTimes() {
        return times;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SOAServiceNode that = (SOAServiceNode) o;
        return Objects.equals(ip, that.ip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip);
    }

    public SOAServiceCenter.NodeProperties getNodeProperties() {
        return nodeProperties;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
