package com.i72.basic;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName PipelineCommand.java
 * @Description TODO
 * @createTime 2022年01月21日 22:46:00
 */
@FunctionalInterface
public interface PipelineCommand<P1, P2> {

    void execute(P1 p1, P2 p2);

}
