package com.i72.basic;

import com.i72.freeway.SpringContextHelper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName basicConfiguration.java
 * @Description TODO
 * @createTime 2021年12月29日 14:43:00
 */
@Configuration
@ConditionalOnBean(BasicMarkter.class)
public class BasicConfiguration {

    @Bean
    public SOAServiceCenter getSOAServiceCenter(){
        return new SOAServiceCenter();
    }

    @Bean
    public SpringContextHelper getSpringContextHelper(){
        return new SpringContextHelper();
    }
}
