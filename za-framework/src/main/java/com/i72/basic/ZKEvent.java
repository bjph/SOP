package com.i72.basic;


import org.springframework.context.ApplicationEvent;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ZKEvent.java
 * @Description TODO
 * @createTime 2022年01月05日 09:59:00
 */
public class ZKEvent extends ApplicationEvent {

    public ZKEvent(Object source) {
        super(source);
    }
}
