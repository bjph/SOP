package com.i72.freeway.api.ums;



import com.i72.freeway.FreewayPath;
import com.i72.freeway.bean.ums.UserResult;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName UmUserService.java
 * @Description TODO
 * @createTime 2022年01月04日 14:12:00
 */
@FreewayPath
public interface UmUserService {


    UserResult getByToken(String token);
    //void logout(String token);
}
