package com.i72.freeway.api.zaorder;



import com.i72.freeway.FreewayPath;
import com.i72.freeway.bean.zaorder.OrderLogisticsRespDTO;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName OrderCenterService.java
 * @Description TODO
 * @createTime 2021年12月29日 15:02:00
 */
@FreewayPath
public interface OrderCenterService {

    /**
     * 根据订单id获取收货信息
     *
     * @param orderId
     * @return
     */
    OrderLogisticsRespDTO orderLogistics(Long orderId);

}
