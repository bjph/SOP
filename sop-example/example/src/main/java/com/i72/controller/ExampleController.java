package com.i72.controller;

import com.alibaba.fastjson.JSON;
import com.gitee.sop.servercommon.annotation.Open;
import com.i72.bean.dto.SayReqDTO;
import com.i72.bean.dto.SayVO;
import com.i72.freeway.api.ums.UmUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;


/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ExampleController.java
 * @Description TODO
 * @createTime 2022年01月11日 10:33:00
 */
@RestController
@Api("XXX业务")
@Slf4j
public class ExampleController {

    //可注入freeway 接口。直通老接口
    @Autowired
    private UmUserService umUserService;

    @ApiOperation("XXX接口")
    @RequestMapping("/say")
    @Open("example.say")
    public SayVO say(SayReqDTO sayReqDTO){

        log.info("参数:{}",JSON.toJSONString(sayReqDTO));

        SayVO sayVO = new SayVO();
        sayVO.setAge(new Integer(new Random().nextInt(100)));
        sayVO.setName(sayReqDTO.getName());
        return sayVO;
    }
}
