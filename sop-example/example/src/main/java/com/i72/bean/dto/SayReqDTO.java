package com.i72.bean.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName SayReqDTO.java
 * @Description TODO
 * @createTime 2022年01月11日 10:40:00
 */
@Data
public class SayReqDTO {

    @ApiModelProperty(value = "姓名", example = "张三")
    @NotNull(message = "姓名不能为空")
    private String name;



}
