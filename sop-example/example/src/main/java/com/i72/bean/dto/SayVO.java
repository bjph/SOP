package com.i72.bean.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName SayVO.java
 * @Description TODO
 * @createTime 2022年01月11日 10:40:00
 */
@Data
public class SayVO {

    @ApiModelProperty(value = "年龄", example = "25")
    private Integer age;

    @ApiModelProperty(value = "姓名",example = "李广")
    private String name;

}
