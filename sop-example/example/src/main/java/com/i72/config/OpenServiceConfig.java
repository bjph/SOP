package com.i72.config;

import com.gitee.sop.servercommon.bean.ServiceConfig;
import com.gitee.sop.servercommon.configuration.AlipayServiceConfiguration;
import com.gitee.sop.servercommon.swagger.SwaggerSupport;
import com.i72.freeway.FreewayMarkter;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 开放平台配置类
 * @author jiangj
 */
@Configuration
public class OpenServiceConfig extends AlipayServiceConfiguration {

    static {
        ServiceConfig.getInstance().getI18nModules().add("i18n/isp/goods_error");
    }


    /**
     * 开启文档，本地微服务文档地址：http://localhost:2222/doc.html
     * http://ip:port/v2/api-docs
     */
    @Configuration
    @EnableSwagger2
    public static class Swagger2 extends SwaggerSupport implements EnvironmentAware {
        private Environment environment = null;
        @Override
        public void setEnvironment(Environment environment) {
            this.environment = environment;
        }

        @Override
        protected String getDocTitle() {
            return environment.getProperty("spring.application.name");
        }

        @Override
        protected boolean swaggerAccessProtected() {
            return false;
        }
    }

    @Bean
    public FreewayMarkter freewayMarkter(){
        return new FreewayMarkter();
    }



}

