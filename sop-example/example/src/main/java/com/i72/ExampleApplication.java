package com.i72;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ExampleApplication.java
 * @Description TODO
 * @createTime 2022年01月11日 10:30:00
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);
    }
}
