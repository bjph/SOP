package com.gitee.sop.storyweb.controller;

import com.gitee.sop.servercommon.annotation.Open;
import com.gitee.sop.storyweb.controller.param.StoryParam;
import com.gitee.sop.storyweb.freeway.api.ums.UmUserService;
import com.gitee.sop.storyweb.freeway.api.zaorder.OrderCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName Example1011.java
 * @Description TODO
 * @createTime 2022年01月08日 22:53:00
 */
@RestController
public class Example1011 {

    @Autowired
    private UmUserService umUserService;

    @Autowired
    private OrderCenterService orderCenterService;

    @Open("story.path.same2")
    @RequestMapping("iam_same_path2")
    public Object iam_same_path(StoryParam param) {
        //umUserService
        //umUserService.getByToken("")
        param.setName(param.getName() + " story..");
        return param;
    }

}
