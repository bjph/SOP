package com.gitee.sop.storyweb.freeway.api.ums;


import com.gitee.sop.storyweb.freeway.bean.ums.UserResult;
import com.i72.freeway.FreewayPath;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName UmUserService.java
 * @Description TODO
 * @createTime 2022年01月04日 14:12:00
 */
@FreewayPath
public interface UmUserService {


    UserResult getByToken(String token);
    //void logout(String token);
}
