package com.gitee.sop.storyweb.freeway.bean.zaorder;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName OrderLogisticsRespDTO.java
 * @Description TODO
 * @createTime 2021年12月29日 15:01:00
 */
@Data
@ToString
public class OrderLogisticsRespDTO implements Serializable {

    /**
     * 收货人
     */
    private String shippingPerson;

    /**
     * 联系方式
     */
    private String shippingPhone;

    /**
     * 收货省
     */
    private String shippingProvince;

    /**
     * 收货市
     */
    private String shippingCity;

    /**
     * 收货区县
     */
    private String shippingCounty;

    /**
     * 详细地址
     */
    private String shippingAddress;

    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区编码
     */
    private String countyCode;

    /**
     * 备注
     */
    private String remark;

    /**
     * 收获国家
     */
    private String shippingCountry;

    /**
     * 国级编码
     */
    private String countryCode;

}
