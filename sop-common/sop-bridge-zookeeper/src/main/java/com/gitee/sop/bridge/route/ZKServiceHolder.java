package com.gitee.sop.bridge.route;

import com.gitee.sop.gatewaycommon.route.ServiceHolder;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ZKServiceHolder.java
 * @Description TODO
 * @createTime 2022年01月05日 10:03:00
 */
public class ZKServiceHolder extends ServiceHolder {

    public ZKServiceHolder(String serviceId, long lastUpdatedTimestamp) {
        super(serviceId, lastUpdatedTimestamp);
    }
}
