package com.gitee.sop.bridge;

import com.gitee.sop.bridge.route.*;
import com.i72.basic.BasicMarkter;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.ServerList;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.netflix.ribbon.ServerIntrospector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName SopRegisterAutoConfiguration.java
 * @Description TODO
 * @createTime 2021年12月27日 10:49:00
 */
@Configuration
public class SopRegisterAutoConfiguration {

    @Bean
    public ZKRegisterListener getZKRegisterListener(){
        return new ZKRegisterListener();
    }

    @Bean
    public BasicMarkter getBasicMarkter(){
        return new BasicMarkter();
    }

    @Bean
    public ZKInit getZKInit(){
        return new ZKInit();
    }

    @Bean
    @ConditionalOnMissingBean
    public ServerIntrospector getZKServerIntrospector(){
        return new ZKServerIntrospector();
    }

    @Bean
    @ConditionalOnMissingBean
    public ServerList getZKServerList(){
        return new ZKServerList();
    }

    @Bean
    @ConditionalOnMissingBean
    public IRule getZKRule(){
        return new ZKRule();
    }
}
