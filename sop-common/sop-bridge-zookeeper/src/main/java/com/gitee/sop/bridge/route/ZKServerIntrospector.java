package com.gitee.sop.bridge.route;

import com.gitee.sop.gatewaycommon.bean.SopConstants;
import com.gitee.sop.gatewaycommon.validate.alipay.StringUtils;
import com.netflix.loadbalancer.Server;
import org.springframework.cloud.netflix.ribbon.DefaultServerIntrospector;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ZKServerIntrospector.java
 * @Description TODO
 * @createTime 2022年01月08日 10:06:00
 */
public class ZKServerIntrospector extends DefaultServerIntrospector {

    @Override
    public Map<String, String> getMetadata(Server server) {



        Map m = new HashMap();

        m.put("serviceName",server.getMetaInfo().getAppName());

        //设置节点环境

        /*
        String env = "pre";

        if(!StringUtils.isEmpty(env)) {

            m.put(SopConstants.METADATA_ENV_KEY,env);
        }*/

        return m;
    }
}
