package com.gitee.sop.bridge.route;

import com.i72.basic.ZKEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.client.discovery.event.HeartbeatEvent;
import org.springframework.context.ApplicationContext;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ZKInit.java
 * @Description TODO
 * @createTime 2022年01月05日 15:39:00
 */
public class ZKInit implements ApplicationRunner {

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        applicationContext.publishEvent(new ZKEvent(this));
    }
}
