package com.gitee.sop.bridge.route;

import com.i72.basic.SOAServiceAddress;
import com.i72.basic.SOAServiceCenter;
import com.i72.basic.SOAServiceNode;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName ZKServerList.java
 * @Description TODO
 * @createTime 2022年01月08日 10:36:00
 */
public class ZKServerList implements ServerList<Server> {

    @Autowired
    private SOAServiceCenter soaServiceCenter;

    @Override
    public List<Server> getInitialListOfServers() {
        return getServer();
    }

    @Override
    public List<Server> getUpdatedListOfServers() {
        return getServer();
    }

    private List<Server> getServer(){
        Map<String, SOAServiceAddress> soaServiceAddressMap = soaServiceCenter.getServiceAddresses();

        if(soaServiceAddressMap!=null && soaServiceAddressMap.size()>0){
            List<Server> serverList = new ArrayList<>();

            soaServiceAddressMap.forEach(new BiConsumer<String, SOAServiceAddress>() {
                @Override
                public void accept(String s, SOAServiceAddress soaServiceAddress) {
                    soaServiceAddress.getNodeListMap().forEach(new BiConsumer<String, List<SOAServiceNode>>() {
                        @Override
                        public void accept(String nodeType, List<SOAServiceNode> soaServiceNodes) {

                            soaServiceNodes.forEach(i->{
                                Server server = null;
                                String ip = "";
                                Integer port = 80;
                                if(i.getIp().contains(":")){
                                    String [] address = i.getIp().split(":");
                                    ip = address[0];
                                    port = new Integer(address[1]);
                                }
                                server = new Server(ip,port){
                                    @Override
                                    public MetaInfo getMetaInfo() {
                                        MetaInfo metaInfo = new MetaInfo() {
                                            @Override
                                            public String getAppName() {
                                                return s;
                                            }

                                            @Override
                                            public String getServerGroup() {
                                                return null;
                                            }

                                            @Override
                                            public String getServiceIdForDiscovery() {
                                                return null;
                                            }

                                            @Override
                                            public String getInstanceId() {
                                                return null;
                                            }
                                        };

                                        return metaInfo;
                                    }
                                };


                                serverList.add(server);

                            });

                        }
                    });
                }
            });

            return serverList;
        }

        return null;
    }
}
