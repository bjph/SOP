package com.gitee.sop.sopauth.mapper;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.gitee.sop.sopauth.entity.IsvInfo;
import com.gitee.sop.sopauth.entity.IsvKeys;

/**
 * @author jiangj
 * @version 1.0.0
 * @ClassName IvsKyesMapper.java
 * @Description TODO
 * @createTime 2022年01月16日 11:07:00
 */
public interface IsvKyesMapper extends CrudMapper<IsvKeys, Long> {
}
