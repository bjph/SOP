package com.gitee.sop.sopauth.auth;

import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URISyntaxException;

/**
 * @author tanghc
 */
public interface AccessTokenService {



    /**
     * 通过appId获取accessToken.
     * @param fetchTokenParam
     * @param authConfig 配置项
     * @return 返回响应内容
     * @throws URISyntaxException
     * @throws OAuthSystemException
     */
    FetchTokenResult accessToken(FetchTokenParam fetchTokenParam, OAuth2Config authConfig);
}
