package com.gitee.sop.sopauth.auth.impl;

import com.gitee.sop.sopauth.auth.AppIdManager;
import com.gitee.sop.sopauth.auth.OpenUser;
import com.gitee.sop.sopauth.auth.TokenPair;
import com.gitee.sop.sopauth.entity.IsvInfo;
import com.gitee.sop.sopauth.entity.IsvKeys;
import com.gitee.sop.sopauth.mapper.IsvInfoMapper;
import com.gitee.sop.sopauth.mapper.IsvKyesMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tanghc
 */
@Service
@Slf4j
public class AppIdManagerImpl implements AppIdManager {

    public static final int FORBIDDEN = 2;

    @Autowired
    private IsvInfoMapper isvInfoMapper;

    @Autowired
    private IsvKyesMapper isvKyesMapper;

    @Override
    public boolean isValidAppId(String appId) {
        IsvInfo isvInfo = isvInfoMapper.getByColumn("app_key", appId);
        if (isvInfo == null) {
            return false;
        }
        if (isvInfo.getStatus().intValue() == FORBIDDEN) {
            log.error("appId已禁用:{}", appId);
            return false;
        }
        return true;
    }

    @Override
    public boolean isValidSecret(String appId, String secret) {
        IsvKeys isvKeys = isvKyesMapper.getByColumn("app_key",appId);
        if(isvKeys!=null){
            if(isvKeys.getSecret().equals(secret)){
                return true;
            }
            return false;
        }

        return false;
    }
    @Override
    public String getOrganId(String appId) {
        IsvKeys isvKeys = isvKyesMapper.getByColumn("app_key",appId);
        if(isvKeys!=null){
            isvKeys.getOrganId();
        }

        return null;
    }

}
