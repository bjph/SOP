package com.gitee.sop.sopauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//@EnableDiscoveryClient
@SpringBootApplication
public class SopAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SopAuthApplication.class, args);
	}

}
