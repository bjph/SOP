package com.gitee.sop.sopauth.auth;

import com.gitee.sop.sopauth.entity.IsvKeys;

/**
 * @author tanghc
 */
public interface AppIdManager {
    /**
     * 是否是合法的appId
     *
     * @param appId
     * @return true:合法
     */
    boolean isValidAppId(String appId);


    boolean isValidSecret(String appId,String secret);
    String getOrganId(String appId);
}
