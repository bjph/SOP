package com.gitee.sop.sopauth.config;

import com.gitee.sop.servercommon.bean.ServiceConfig;
import com.gitee.sop.servercommon.configuration.AlipayServiceConfiguration;
import com.i72.basic.BasicMarkter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 使用支付宝开放平台功能
 *
 * @author tanghc
 */
@Configuration
public class OpenServiceConfig extends AlipayServiceConfiguration {


    static {
        ServiceConfig.getInstance().getI18nModules().add("i18n/isp/goods_error");
    }


    @Configuration
    @ConditionalOnClass(name = "com.i72.basic.BasicConfiguration")
    class ZKConfiguration{
        @Bean
        @ConditionalOnMissingBean
        public BasicMarkter ZKManager(){
            return new BasicMarkter();
        }
    }
}
